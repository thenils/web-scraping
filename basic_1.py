import requests

# res = requests.get('http://www.ecolabelindex.com/')

# print(res.text)
# print(res.status_code)

from bs4 import BeautifulSoup
import urllib.request, urllib.parse, urllib.error

url = 'http://www.ecolabelindex.com/ecolabels/ecolabel/80-plus'

res = requests.get(url)

soup = BeautifulSoup(res.text, 'html.parser')
tags = soup('p')
data = []
for tag in tags:
	data.append(tag.get('content'))
print(data)